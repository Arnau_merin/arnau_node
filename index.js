var express = require("express");
var app = express();

var morgan = require('morgan');
var bodyParser = require('body-parser');
var serveStatic = require('serve-static');
const fileType = require('file-type');


var posts = [];
var postId = 0;

app.use(serveStatic('public', { 'index': ['default.html', 'default.htm'] }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());
app.use(morgan('combined'));

app.post("/removeduplicatewords",function(req,res){
    //var palabras = [ "hola","como","estas"];

    if(req.body.palabras!=null){
        
        //https://stackoverflow.com/ esta muy bien para sacar funciones así
        //Solo el comprobar si una palabra esta repetida en un string
        var palabras=[];
        var palabraFinal='';
        var palabraComp='';
        for(var i=0;req.body.palabras.length>i;i++){            
            if(req.body.palabras[i] == ','){                
                var  comp = false;
                for(var j=0;palabras.length>j;j++){
                    if(palabraComp==palabras[j]){comp = true;}
                }
                if(!comp){palabras.push(palabraComp);}
                palabraComp='';
            }else{ palabraComp += req.body.palabras[i]; }
        }
        for(var i=0;palabras.length>i;i++){
            palabraFinal+=palabras[i];
            if(i<palabras.length-1){ palabraFinal+=',' }
        } 
        res.write(palabraFinal);
    }
    res.end();
})


app.post('/detectfiletype',function(req,res){
    
    //Supongo q algo hace pero peta
    if(req.body.url == null){
        //Posa una URL Valida     
    }else{
        const http = require('https'); 
        const url = req.body.url;
        var stringJson;
        http.get(url, response => {
            response.on('readable', () => {
            const chunk = response.read(fileType.minimumBytes);
            response.destroy();
            //console.log('file: ',fileType(chunk));
            stringJson=(JSON.stringify(fileType(chunk)));
            console.log('file:',stringJson);                    
            });
        });
    }    
    res.end()
});

var theOrder=[];

app.post('/botorder/:order',function(req,res){
    if(req.params.order == null){
        res.send("NOT OK");
    }
    else{
        res.send('OK');
        var order = false;
        for(var i = 0; theOrder.length > i; i++){
            if(theOrder[i].orderName == req.params.order){
                theOrder[i].botorder = req.body.botorder;
                order = true;
                var newOrder = {orderName:req.params.order,botorder:req.body.botorder};
                theOrder.push(newOrder);
            }
        }
        if(!order){
            //EL NONE (Descomenta esto y comenta el otro)
            //var newOrder = {orderName:req.params.order,botorder:'NONE'};
            //EL KILLMATRIX (Descomenta esto y comenta el otro)
            var newOrder = {orderName:req.params.order,botorder:req.body.botorder};
            theOrder.push(newOrder);
        }
    }
    res.end();
});

app.get('/botorder/:order',function(req,res){
    if(req.params.order == null){        
        res.send("NOT OK");        
    }else{
        var actualOrder = {orderName:req.params.order,botorder:'NONE'};
        for(var i = 0; theOrder.length > i; i++){            
            if(theOrder[i].orderName==actualOrder.orderName){ actualOrder.botorder=theOrder[i].botorder; }
        }
        res.write(actualOrder.botorder);
    }
    res.end()
});







app.listen(8000);